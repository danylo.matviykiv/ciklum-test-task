package com.trendyol.services;

import com.trendyol.services.converter.ConverterFactory;
import com.trendyol.services.converter.HomeUrlConverter;
import com.trendyol.services.converter.ProductUrlConverter;
import com.trendyol.services.converter.SearchUrlConverter;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import static com.trendyol.services.converter.UrlConstants.*;
import static org.junit.Assert.assertEquals;

public class ConverterFactoryTest {

    private static final String TURKISH_PATH = "/Muğla-Sıtkı/Koçman-Üniversitesi";

    private ConverterFactory converterFactory;

    @Before
    public void before() {
        this.converterFactory = new ConverterFactory();
    }

    @Test
    public void getUrlConverterTest() {
        var prodConverter1 = converterFactory.getUrlConverter(prepareProductWebUrl());
        var prodConverter2 = converterFactory.getUrlConverter(prepareProductDeepLink());

        var searchConverter1 = converterFactory.getUrlConverter(prepareSearchWebUrl());
        var searchConverter2 = converterFactory.getUrlConverter(prepareSearchDeepLink());

        var homeConverter = converterFactory.getUrlConverter(prepareHomeLink());

        assertEquals(ProductUrlConverter.class, prodConverter1.getClass());
        assertEquals(ProductUrlConverter.class, prodConverter2.getClass());
        assertEquals(SearchUrlConverter.class, searchConverter1.getClass());
        assertEquals(SearchUrlConverter.class, searchConverter2.getClass());
        assertEquals(HomeUrlConverter.class, homeConverter.getClass());
    }

    private UriComponents prepareProductWebUrl() {
        var path = TURKISH_PATH + WEB_URL_PRODUCT + 1925865;
        return UriComponentsBuilder.newInstance()
                .scheme(WEB_URL_SCHEME)
                .host(WEB_URL_TRENDYOL)
                .path(path)
                .queryParam(WEB_URL_BOUTIQUE_ID, 439892)
                .queryParam(WEB_URL_MERCHANT_ID, 105064)
                .build();
    }

    private UriComponents prepareProductDeepLink() {
        return UriComponentsBuilder.newInstance()
                .scheme(DEEP_LINK_SCHEME)
                .host(StringUtils.EMPTY)
                .queryParam(DEEP_LINK_PAGE, DEEP_LINK_PRODUCT)
                .queryParam(DEEP_LINK_CONTENT_ID, 1925865)
                .build();
    }

    private UriComponents prepareSearchWebUrl() {
        return UriComponentsBuilder.newInstance()
                .scheme(WEB_URL_SCHEME)
                .host(WEB_URL_TRENDYOL)
                .path(WEB_URL_SEARCH_PATH)
                .queryParam(WEB_URL_QUERY, "elbise")
                .build();
    }

    private UriComponents prepareSearchDeepLink() {
        return UriComponentsBuilder.newInstance()
                .scheme(DEEP_LINK_SCHEME)
                .host(StringUtils.EMPTY)
                .queryParam(DEEP_LINK_PAGE, DEEP_LINK_SEARCH)
                .queryParam(DEEP_LINK_QUERY, "%C3%BCt%C3%BC")
                .build();
    }

    private UriComponents prepareHomeLink() {
        return UriComponentsBuilder.newInstance()
                .scheme(DEEP_LINK_SCHEME)
                .host(StringUtils.EMPTY)
                .queryParam(DEEP_LINK_PAGE, "ORDERS")
                .build();
    }
}
