package com.trendyol.services;

import com.trendyol.services.converter.ProductUrlConverter;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

import static com.trendyol.services.converter.UrlConstants.*;

@RunWith(MockitoJUnitRunner.class)
public class ProductUrlConverterTest {

    private static final String TURKISH_SYMBOLS = "/Muğla-Sıtkı-Koçman-Üniversitesi";
    private static final String BRAND_PATH = "/brand/";

    @InjectMocks
    private ProductUrlConverter productUrlConverter;

    private Map<UriComponents, UriComponents> resultMap;

    @Test
    public void toDeepLinkTest() {
        prepareResultMap(TURKISH_SYMBOLS);
        resultMap.forEach((uriComponents, expectedResult) -> {
            var actualResult = productUrlConverter.toDeepLink(uriComponents);
            Assert.assertEquals(expectedResult.toUriString(), actualResult);
        });
    }

    @Test
    public void toWebUrlTest() {
        prepareResultMap("name");
        resultMap.forEach((expectedResult, uriComponents) -> {
            var actualResult = productUrlConverter.toWebUrl(uriComponents);
            Assert.assertEquals(expectedResult.toUriString(), actualResult);
        });
    }

    public void prepareResultMap(String brandName) {
        this.resultMap = new HashMap<>();
        IntStream.range(0, 5).forEach(i -> {
            int productId = 100_000 + (int) (Math.random() * 8_000_000);
            int boutiqueId = 10_000 + (int) (Math.random() * 800_000);
            int merchantId = 10_000 + (int) (Math.random() * 800_000);

            var path = BRAND_PATH + brandName + WEB_URL_PRODUCT + productId;
            var webUrlBuilder = UriComponentsBuilder.newInstance()
                    .scheme(WEB_URL_SCHEME)
                    .host(WEB_URL_TRENDYOL)
                    .path(path);

            var webUrl = webUrlBuilder.cloneBuilder()
                    .queryParam(WEB_URL_BOUTIQUE_ID, boutiqueId)
                    .queryParam(WEB_URL_MERCHANT_ID, merchantId)
                    .build();
            var webUrlWithoutBoutique = webUrlBuilder.cloneBuilder()
                    .queryParam(WEB_URL_MERCHANT_ID, merchantId).build();
            var webUrlWithoutMerchant = webUrlBuilder.cloneBuilder()
                    .queryParam(WEB_URL_BOUTIQUE_ID, boutiqueId).build();

            var deepLinkBuilder = UriComponentsBuilder.newInstance()
                    .scheme(DEEP_LINK_SCHEME)
                    .host(StringUtils.EMPTY)
                    .queryParam(DEEP_LINK_PAGE, DEEP_LINK_PRODUCT)
                    .queryParam(DEEP_LINK_CONTENT_ID, productId);
            var deepLink = deepLinkBuilder.cloneBuilder()
                    .queryParam(DEEP_LINK_CAMPAIGN_ID, boutiqueId)
                    .queryParam(DEEP_LINK_MERCHANT_ID, merchantId)
                    .build();

            var deepLinkWithoutCampaign = deepLinkBuilder.cloneBuilder()
                    .queryParam(DEEP_LINK_MERCHANT_ID, merchantId).build();
            var deepLinkWithoutMerchant = deepLinkBuilder.cloneBuilder()
                    .queryParam(DEEP_LINK_CAMPAIGN_ID, boutiqueId).build();

            resultMap.put(webUrl, deepLink);
            resultMap.put(webUrlWithoutBoutique, deepLinkWithoutCampaign);
            resultMap.put(webUrlWithoutMerchant, deepLinkWithoutMerchant);
        });
    }
}
