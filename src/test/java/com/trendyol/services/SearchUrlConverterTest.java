package com.trendyol.services;

import com.trendyol.services.converter.SearchUrlConverter;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.trendyol.services.converter.UrlConstants.*;

@RunWith(MockitoJUnitRunner.class)
public class SearchUrlConverterTest {

    private static final String TURKISH_SYMBOLS = "Muğla-Sıtkı-Koçman-Üniversitesi";

    @InjectMocks
    private SearchUrlConverter searchUrlConverter;

    private Map<UriComponents, UriComponents> resultMap;

    @Before
    public void before() {
        prepareResultMap();
    }

    @Test
    public void toDeepLinkTest() {
        resultMap.forEach((uriComponents, expectedResult) -> {
            var actualResult = searchUrlConverter.toDeepLink(uriComponents);
            Assert.assertEquals(expectedResult.toUriString(), actualResult);
        });
    }

    @Test
    public void toWebUrlTest() {
        resultMap.forEach((expectedResult, uriComponents) -> {
            var actualResult = searchUrlConverter.toWebUrl(uriComponents);
            Assert.assertEquals(expectedResult.toUriString(), actualResult);
        });
    }

    private void prepareResultMap() {
        this.resultMap = new HashMap<>();
        List<String> queryList = Arrays.asList("elbise", "%C3%BCt%C3%BC", "test", "ŞŞÖ", TURKISH_SYMBOLS);
        queryList.forEach(q -> {

            var webUrl = UriComponentsBuilder.newInstance()
                    .scheme(WEB_URL_SCHEME)
                    .host(WEB_URL_TRENDYOL)
                    .path(WEB_URL_SEARCH_PATH)
                    .queryParam(WEB_URL_QUERY, q)
                    .build();

            var deepLink = UriComponentsBuilder.newInstance()
                    .scheme(DEEP_LINK_SCHEME)
                    .host(StringUtils.EMPTY)
                    .queryParam(DEEP_LINK_PAGE, DEEP_LINK_SEARCH)
                    .queryParam(DEEP_LINK_QUERY, q)
                    .build();

            resultMap.put(webUrl, deepLink);
        });
    }
}
