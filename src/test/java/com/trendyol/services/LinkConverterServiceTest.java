package com.trendyol.services;

import com.trendyol.dto.LinkDto;
import com.trendyol.model.Link;
import com.trendyol.repository.LinkRepository;
import com.trendyol.services.converter.ConverterFactory;
import com.trendyol.services.converter.ProductUrlConverter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.util.UriComponents;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LinkConverterServiceTest {

    public static String TEST_WEB_URL = "https://www.trendyol.com/sr?q=elbise";
    public static String TEST_DEEP_LINK = "ty://?Page=Search&Query=elbise";

    @InjectMocks
    private LinkConverterService linkConverterService;

    @Mock
    private ConverterFactory converterFactory;

    @Mock
    private LinkRepository linkRepository;

    @Mock
    private ProductUrlConverter urlConverter;

    private LinkDto linkDto;

    @Before
    public void before() {
        linkDto = prepareLinkDto();
        when(converterFactory.getUrlConverter(any(UriComponents.class))).thenReturn(urlConverter);
        when(linkRepository.save(any(Link.class))).thenAnswer(i -> i.getArguments()[0]);
    }

    @Test
    public void toDeepLinkTest() {
        when(urlConverter.toDeepLink(any(UriComponents.class))).thenReturn(TEST_DEEP_LINK);
        var actualResult = linkConverterService.getDeepLink(linkDto);
        Assert.assertEquals(TEST_DEEP_LINK, actualResult.getDeepLink());
    }

    @Test
    public void toWebUrlTest() {
        when(urlConverter.toWebUrl(any(UriComponents.class))).thenReturn(TEST_WEB_URL);
        var actualResult = linkConverterService.getWebUrl(linkDto);
        Assert.assertEquals(TEST_WEB_URL, actualResult.getWebUrl());
    }

    private LinkDto prepareLinkDto() {
        return LinkDto.builder()
                .deepLink(TEST_DEEP_LINK)
                .webUrl(TEST_WEB_URL)
                .build();
    }
}
