package com.trendyol.services;

import com.trendyol.services.converter.HomeUrlConverter;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.trendyol.services.converter.UrlConstants.*;

@RunWith(MockitoJUnitRunner.class)
public class HomeUrlConverterTest {

    private static final String HOME_WEB_URL = "https://www.trendyol.com";
    private static final String HOME_DEEP_LINK = "ty://?Page=Home";
    private static final String TURKISH_SYMBOLS_PATH = "/Muğla/Sıtkı/Koçman/Üniversitesi";

    @InjectMocks
    private HomeUrlConverter homeUrlConverter;

    private Map<String, UriComponents> deepLinkMap;
    private Map<String, UriComponents> webUrlMap;

    @Test
    public void toDeepLinkTest() {
        prepareWebUrlLinks();
        webUrlMap.forEach((expectedResult, uriComponents) -> {
            var actualResult = homeUrlConverter.toDeepLink(uriComponents);
            Assert.assertEquals(expectedResult, actualResult);
        });
    }

    @Test
    public void toWebUrlTest() {
        prepareDeepLinks();
        deepLinkMap.forEach((expectedResult, uriComponents) -> {
            var actualResult = homeUrlConverter.toWebUrl(uriComponents);
            Assert.assertEquals(expectedResult, actualResult);
        });
    }

    private void prepareWebUrlLinks() {
        this.webUrlMap = new HashMap<>();
        List<String> webUrlPathList = Arrays.asList("/Hesabim/Favoriler", "/Hesabim/#/Siparisleri",
                "/test", TURKISH_SYMBOLS_PATH);
        webUrlPathList.forEach(path -> {
            var webUrl = UriComponentsBuilder.newInstance()
                    .scheme(WEB_URL_SCHEME)
                    .host(WEB_URL_TRENDYOL)
                    .path(path)
                    .build();
            webUrlMap.put(HOME_DEEP_LINK, webUrl);
        });
    }

    private void prepareDeepLinks() {
        this.deepLinkMap = new HashMap<>();
        List<String> deepLinkPathList = Arrays.asList("Favorites", "Order", "test");
        deepLinkPathList.forEach(p -> {
            var deepLink = UriComponentsBuilder.newInstance()
                    .scheme(DEEP_LINK_SCHEME)
                    .queryParam(DEEP_LINK_PAGE, p)
                    .build();
            deepLinkMap.put(HOME_WEB_URL, deepLink);
        });
    }
}
