package com.trendyol.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trendyol.dto.LinkDto;
import com.trendyol.model.Link;
import com.trendyol.services.LinkConverterService;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static com.trendyol.services.converter.UrlConstants.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(LinkConverterController.class)
public class LinkConverterControllerTest {

    private static final String API_LINK_CONVERTER = "/links";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private LinkConverterService service;

    private ObjectMapper objectMapper;

    private Link link;
    private LinkDto linkDto;

    @Before
    public void before() {
        objectMapper = new ObjectMapper();
        link = prepareLink();
        linkDto = prepareLinkDto();
    }

    @Test
    public void toDeepLinkTest() throws Exception {
        var apiPath = API_LINK_CONVERTER + "/deep-link";

        when(service.getDeepLink(linkDto)).thenReturn(link);

        var response = this.mockMvc.perform(post(apiPath)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(linkDto)))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();
        var resultLink = objectMapper.readValue(response, Link.class);
        assertLinks(resultLink, link);
    }

    @Test
    public void toWebUrlTest() throws Exception {
        var apiPath = API_LINK_CONVERTER + "/web-url";

        when(service.getWebUrl(linkDto)).thenReturn(link);

        var response = this.mockMvc.perform(post(apiPath)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(linkDto)))
                .andReturn().getResponse().getContentAsString();
        var resultLink = objectMapper.readValue(response, Link.class);
        assertLinks(resultLink, link);
    }

    private void assertLinks(Link expectedResult, Link actualResult) {
        assertEquals(expectedResult.getDeepLink(), actualResult.getDeepLink());
        assertEquals(expectedResult.getWebUrl(), actualResult.getWebUrl());
    }

    private LinkDto prepareLinkDto() {
        var webUrl = UriComponentsBuilder.newInstance()
                .scheme(WEB_URL_SCHEME)
                .host(WEB_URL_TRENDYOL)
                .path("butik/liste/erkek")
                .build().toUriString();

        var deepLink = UriComponentsBuilder.newInstance()
                .scheme(DEEP_LINK_SCHEME)
                .host(StringUtils.EMPTY)
                .queryParam(DEEP_LINK_PAGE, DEEP_LINK_HOME)
                .queryParam(DEEP_LINK_SECTION_ID, 2)
                .build().toUriString();

        return LinkDto.builder()
                .webUrl(webUrl)
                .deepLink(deepLink)
                .build();
    }

    private Link prepareLink() {
        var linkDto = prepareLinkDto();
        var date = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
        return new Link(date, linkDto.getWebUrl(), linkDto.getDeepLink());
    }

}
