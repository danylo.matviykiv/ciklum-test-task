package com.trendyol.exception;

/**
 * Exception for invalid request url
 *
 * @author mdan@ciklum.com
 */
public class InvalidUrlException extends RuntimeException {

    public InvalidUrlException() {
        super("Exception! URL is not valid!");
    }
}
