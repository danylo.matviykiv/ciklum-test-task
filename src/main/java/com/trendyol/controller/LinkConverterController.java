package com.trendyol.controller;

import com.trendyol.dto.LinkDto;
import com.trendyol.model.Link;
import com.trendyol.services.LinkConverterService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * API for converting web-url to deepLink and deepLink to web-url.
 *
 * @author mdan@ciklum.com
 * @see LinkConverterService
 */

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("links")
public class LinkConverterController {

    private static final String LOG_MESSAGE = "Endpoint - {}() call";

    private final LinkConverterService linkConverterService;

    /**
     * Converts web-url to deeplink
     *
     * @param webLink contains web-url
     * @return converted deepLink
     */
    @PostMapping("/deep-link")
    @ResponseStatus(HttpStatus.CREATED)
    public Link toDeepLink(@RequestBody LinkDto webLink) {
        log.info(LOG_MESSAGE, "toDeepLink");
        return linkConverterService.getDeepLink(webLink);
    }

    /**
     * Converts deepLink to web-url
     *
     * @param webLink contains deepLink
     * @return converted web-url
     */
    @PostMapping("/web-url")
    @ResponseStatus(HttpStatus.CREATED)
    public Link toWebUrl(@RequestBody LinkDto webLink) {
        log.info(LOG_MESSAGE, "toWebUrl");
        return linkConverterService.getWebUrl(webLink);
    }
}
