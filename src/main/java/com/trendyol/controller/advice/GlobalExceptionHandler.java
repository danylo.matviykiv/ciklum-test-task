package com.trendyol.controller.advice;

import com.trendyol.dto.ErrorResponseDto;
import com.trendyol.exception.InvalidUrlException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

/**
 * Handler for Exceptions
 *
 * @author mdan@ciklum.com
 * @see InvalidUrlException
 */
@RestControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {

    /**
     * Handles invalid url request exceptions
     *
     * @param invalidUrlException request url exception
     * @return error response
     */
    @ExceptionHandler({InvalidUrlException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorResponseDto> handleException(InvalidUrlException invalidUrlException) {
        var response = prepareErrorResponse(invalidUrlException.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles server errors
     *
     * @param exception server exception
     * @return error response
     */
    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ErrorResponseDto> handleException(Exception exception) {
        var response = prepareErrorResponse(exception.getMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ErrorResponseDto prepareErrorResponse(String message) {
        return ErrorResponseDto.builder()
                .exception(message)
                .dateTime(LocalDateTime.now())
                .build();
    }
}
