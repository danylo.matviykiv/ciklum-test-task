package com.trendyol.repository;

import com.trendyol.model.Link;
import com.trendyol.model.LinkPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * JPA Repository for Link entity
 *
 * @author mdan@ciklum.com
 * @see Link
 */
@Repository
public interface LinkRepository extends JpaRepository<Link, LinkPK> {

    Link findByDeepLink(String deepLink);

    Link findByWebUrl(String webUrl);
}
