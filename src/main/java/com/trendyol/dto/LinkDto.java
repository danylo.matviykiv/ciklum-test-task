package com.trendyol.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Link Data Transfer Object
 *
 * @author mdan@ciklum.com
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LinkDto {

    private String webUrl;
    private String deepLink;
}
