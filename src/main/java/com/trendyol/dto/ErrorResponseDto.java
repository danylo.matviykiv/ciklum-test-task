package com.trendyol.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * Errors response wrapper for exception handler
 *
 * @author mdan@ciklum.com
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponseDto {

    private String exception;
    private LocalDateTime dateTime;
}
