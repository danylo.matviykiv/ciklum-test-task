package com.trendyol.services;

import com.trendyol.dto.LinkDto;
import com.trendyol.exception.InvalidUrlException;
import com.trendyol.model.Link;
import com.trendyol.repository.LinkRepository;
import com.trendyol.services.converter.ConverterFactory;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Objects;

import static com.trendyol.services.converter.UrlConstants.*;

/**
 * Service class which contains methods responsible for converting webUrl to deepLink and reverse.
 *
 * @author mdan@ciklum.com
 * @see com.trendyol.controller.LinkConverterController
 */

@Slf4j
@Service
@AllArgsConstructor
public class LinkConverterService {

    private final LinkRepository linkRepository;
    private final ConverterFactory converterFactory;

    /**
     * Checking for deepLink in database by webUrl.
     * If deepLink is not found, convert webUrl to deepLink and save in database.
     *
     * @param linkDto - contains web-url link.
     * @return Link that contains deepLink.
     */
    public Link getDeepLink(LinkDto linkDto) {
        var webUrl = linkDto.getWebUrl();
        var link = linkRepository.findByWebUrl(webUrl);

        if (Objects.isNull(link))
            link = toDeepLink(webUrl);

        return link;
    }

    /**
     * Checking for webUrl in database by deepLink.
     * If webUrl is not found, convert deepLink to webUrl and save in database.
     *
     * @param linkDto - contains deepLink link.
     * @return Link that contains web-url.
     */
    public Link getWebUrl(LinkDto linkDto) {
        var deepLink = linkDto.getDeepLink();
        var link = linkRepository.findByDeepLink(deepLink);

        if (Objects.isNull(link))
            link = toWebUrl(deepLink);

        return link;
    }

    /**
     * Converting deepLink to webUrl and saving both in database.
     *
     * @param deepLink from request.
     * @return Link that contains web-url.
     */
    private Link toWebUrl(String deepLink) {
        var uriComponents = UriComponentsBuilder
                .fromUriString(deepLink)
                .build();

        validateDeepLink(uriComponents);
        var urlConverter = converterFactory.getUrlConverter(uriComponents);
        var webUrl = urlConverter.toWebUrl(uriComponents);

        return saveLink(webUrl, deepLink);
    }

    /**
     * Converting webUrl to deepLink and saving both in database.
     *
     * @param webUrl from request.
     * @return Link that contains deepLink.
     */
    private Link toDeepLink(String webUrl) {
        var uriComponents = UriComponentsBuilder
                .fromUriString(webUrl)
                .build();

        validateWebUrl(uriComponents);
        var urlConverter = converterFactory.getUrlConverter(uriComponents);
        var deepLink = urlConverter.toDeepLink(uriComponents);

        return saveLink(webUrl, deepLink);
    }

    /**
     * Saving link which contains webUrl and deepLink
     */
    private Link saveLink(String webUrl, String deepLink) {
        var link = new Link();
        link.setDeepLink(deepLink);
        link.setWebUrl(webUrl);
        return linkRepository.save(link);
    }

    /**
     * Validation of WebUrl scheme, host and path.
     *
     * @param uriComponents contains scheme, host and path which have to be validated.
     */
    private void validateWebUrl(UriComponents uriComponents) {
        log.info("Validating web url...");
        var scheme = uriComponents.getScheme();
        var host = uriComponents.getHost();
        var path = uriComponents.getPath();

        if (Objects.isNull(scheme) || Objects.isNull(host) || Objects.isNull(path)
                || !scheme.equals(WEB_URL_SCHEME) || !host.equals(WEB_URL_TRENDYOL))
            throw new InvalidUrlException();
    }

    /**
     * Validation of DeepLink scheme.
     *
     * @param uriComponents contains scheme which have to be validated.
     */
    private void validateDeepLink(UriComponents uriComponents) {
        log.info("Validating deep link...");
        var scheme = uriComponents.getScheme();

        if (Objects.isNull(scheme) || !scheme.equals(DEEP_LINK_SCHEME))
            throw new InvalidUrlException();
    }
}
