package com.trendyol.services.converter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import static com.trendyol.services.converter.UrlConstants.DEEP_LINK_HOME;
import static com.trendyol.services.converter.UrlConstants.DEEP_LINK_PAGE;

/**
 * UrlConverter implementation for converting webUrl to deepLink and reverse using home format.
 *
 * @author mdan@ciklum.com
 * @see com.trendyol.services.converter.UrlConverter
 */
@Slf4j
@Component
public class HomeUrlConverter implements UrlConverter {

    @Override
    public String toDeepLink(UriComponents uriComponents) {
        log.info("Converting webUrl to deepLink using HomeUrlConverter");
        var builder = UriComponentsBuilder.newInstance()
                .queryParam(DEEP_LINK_PAGE, DEEP_LINK_HOME);
        return toDeepLink(builder);
    }

    @Override
    public String toWebUrl(UriComponents uriComponents) {
        log.info("Converting deepLink to webUrl using HomeUrlConverter");
        return toWebUrl(UriComponentsBuilder.newInstance());
    }
}
