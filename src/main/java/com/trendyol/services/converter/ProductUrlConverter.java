package com.trendyol.services.converter;

import com.trendyol.exception.InvalidUrlException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Objects;
import java.util.regex.Pattern;

import static com.trendyol.services.converter.UrlConstants.*;

/**
 * UrlConverter implementation for converting webUrl to deepLink and reverse using product format.
 *
 * @author mdan@ciklum.com
 * @see com.trendyol.services.converter.UrlConverter
 */
@Slf4j
@Component
public class ProductUrlConverter implements UrlConverter {

    private static final String BRAND = "/brand";
    private static final String NAME = "/name";
    private static final String PRODUCT_PREFIX = "-p-";
    private static final String CONTENT_ID_REGEX = ".*-p-(\\d+)";
    private static final Integer CONTENT_ID_INDEX = 1;

    @Override
    public String toDeepLink(UriComponents uriComponents) {
        log.info("Converting webUrl to deepLink using ProductUrlConverter");

        var pattern = Pattern.compile(CONTENT_ID_REGEX);
        var matcher = pattern.matcher(Objects.requireNonNull(uriComponents.getPath()));

        if (!matcher.find())
            throw new InvalidUrlException();

        var contentId = matcher.group(CONTENT_ID_INDEX);
        var builder = UriComponentsBuilder.newInstance()
                .queryParam(DEEP_LINK_PAGE, DEEP_LINK_PRODUCT)
                .queryParam(DEEP_LINK_CONTENT_ID, contentId);
        log.info("Parsed content_id, uri: {}", builder.build().toUriString());

        uriComponents.getQueryParams().forEach((p, v) -> {
            if (p.equals(WEB_URL_BOUTIQUE_ID))
                builder.queryParam(DEEP_LINK_CAMPAIGN_ID, v.get(0));
            else if (p.equals(WEB_URL_MERCHANT_ID))
                builder.queryParam(DEEP_LINK_MERCHANT_ID, v.get(0));
        });
        log.info("Parsed query params, uri: {}", builder.build().toUriString());

        return toDeepLink(builder);
    }

    @Override
    public String toWebUrl(UriComponents uriComponents) {
        log.info("Converting deepLink to webUrl using ProductUrlConverter");
        var params = uriComponents.getQueryParams();
        var page = Objects.requireNonNull(params.getFirst(DEEP_LINK_PAGE));
        var contentId = Objects.requireNonNull(params.getFirst(DEEP_LINK_CONTENT_ID));
        var path = BRAND + NAME + PRODUCT_PREFIX + contentId;

        if (!page.equals(DEEP_LINK_PRODUCT))
            throw new InvalidUrlException();

        var builder = UriComponentsBuilder.newInstance()
                .path(path);
        log.info("Parsed content_id, uri: {}", builder.build().toUriString());

        params.forEach((p, v) -> {
            if (p.equals(DEEP_LINK_CAMPAIGN_ID))
                builder.queryParam(WEB_URL_BOUTIQUE_ID, v.get(0));
            else if (p.equals(DEEP_LINK_MERCHANT_ID))
                builder.queryParam(WEB_URL_MERCHANT_ID, v.get(0));
        });
        log.info("Parsed query params, uri: {}", builder.build().toUriString());

        return toWebUrl(builder);
    }
}
