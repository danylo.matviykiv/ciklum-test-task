package com.trendyol.services.converter;

/**
 * Class for storing constants related to link converting
 *
 * @author mdan@ciklum.com
 */
public class UrlConstants {

    public static final String DEEP_LINK_SCHEME = "ty";
    public static final String DEEP_LINK_PAGE = "Page";
    public static final String DEEP_LINK_HOME = "Home";
    public static final String DEEP_LINK_QUERY = "Query";
    public static final String DEEP_LINK_SEARCH = "Search";
    public static final String DEEP_LINK_PRODUCT = "Product";
    public static final String DEEP_LINK_SECTION_ID = "SectionId";
    public static final String DEEP_LINK_CONTENT_ID = "ContentId";
    public static final String DEEP_LINK_CAMPAIGN_ID = "CampaignId";
    public static final String DEEP_LINK_MERCHANT_ID = "MerchantId";

    public static final String WEB_URL_SCHEME = "https";
    public static final String WEB_URL_QUERY = "q";
    public static final String WEB_URL_SEARCH_PATH = "/sr";
    public static final String WEB_URL_PRODUCT = "-p-";
    public static final String WEB_URL_BOUTIQUE_ID = "boutiqueId";
    public static final String WEB_URL_MERCHANT_ID = "merchantId";
    public static final String WEB_URL_TRENDYOL = "www.trendyol.com";
    public static final String WEB_URL_PRODUCT_REGEX = ".*-p-(\\d+)";

    private UrlConstants() {
        throw new IllegalStateException("Utility class");
    }
}
