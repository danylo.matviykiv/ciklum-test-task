package com.trendyol.services.converter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;

import java.util.Objects;

import static com.trendyol.services.converter.UrlConstants.*;

/**
 * Simple factory pattern implementation for creating different Url Converters.
 *
 * @author mdan@ciklum.com
 * @see UrlConverter
 */

@Slf4j
@Component
public class ConverterFactory {

    private static final String LOG_MESSAGE = "{} returned";

    /**
     * Check path or query param identifier and return appropriate Converter.
     *
     * @param uriComponents - contain identifier inside path or query params (prefix, queryParams etc.)
     * @return One of three UrlConverter child classes (Product, Search, Home) based on url path.
     */
    public UrlConverter getUrlConverter(UriComponents uriComponents) {
        var path = uriComponents.getPath();
        var param = uriComponents.getQueryParams().getFirst(DEEP_LINK_PAGE);

        if (isProduct(path, param)) {
            log.info(LOG_MESSAGE, ProductUrlConverter.class.getSimpleName());
            return new ProductUrlConverter();
        }
        if (isSearch(path, param)) {
            log.info(LOG_MESSAGE, SearchUrlConverter.class.getSimpleName());
            return new SearchUrlConverter();
        }

        log.info(LOG_MESSAGE, HomeUrlConverter.class.getSimpleName());
        return new HomeUrlConverter();
    }

    private boolean isProduct(String path, String param) {
        return Objects.nonNull(path) && path.matches(WEB_URL_PRODUCT_REGEX)
                || Objects.nonNull(param) && param.equals(DEEP_LINK_PRODUCT);
    }

    private boolean isSearch(String path, String param) {
        return (Objects.nonNull(path)) && path.equals(WEB_URL_SEARCH_PATH)
                || Objects.nonNull(param) && param.equals(DEEP_LINK_SEARCH);
    }
}
