package com.trendyol.services.converter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import static com.trendyol.services.converter.UrlConstants.*;

/**
 * UrlConverter implementation for converting webUrl to deepLink and reverse using search format.
 *
 * @author mdan@ciklum.com
 * @see com.trendyol.services.converter.UrlConverter
 */
@Slf4j
@Component
public class SearchUrlConverter implements UrlConverter {

    @Override
    public String toDeepLink(UriComponents uriComponents) {
        log.info("Converting webUrl to deepLink using SearchUrlConverter");
        var query = uriComponents.getQueryParams().getFirst(WEB_URL_QUERY);
        var builder = UriComponentsBuilder.newInstance()
                .queryParam(DEEP_LINK_PAGE, DEEP_LINK_SEARCH)
                .queryParam(DEEP_LINK_QUERY, query);
        return toDeepLink(builder);
    }

    @Override
    public String toWebUrl(UriComponents uriComponents) {
        log.info("Converting deepLink to webUrl using SearchUrlConverter");
        var query = uriComponents.getQueryParams().getFirst(DEEP_LINK_QUERY);
        var builder = UriComponentsBuilder.newInstance()
                .path(WEB_URL_SEARCH_PATH)
                .queryParam(WEB_URL_QUERY, query);
        return toWebUrl(builder);
    }
}
