package com.trendyol.model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

/**
 * Entity which contains link in deepLink and webUrl formats.
 *
 * @author mdan@ciklum.com
 */

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@IdClass(LinkPK.class)
public class Link {

    @Column
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @Id
    private String webUrl;
    @Id
    private String deepLink;
}
