package com.trendyol.model;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Private key for link entity
 *
 * @author mdan@ciklum.com
 * @see Link
 */

@Embeddable
@EqualsAndHashCode
@RequiredArgsConstructor
public class LinkPK implements Serializable {

    private String webUrl;
    private String deepLink;
}
