FROM openjdk:13.0.2-slim-buster
WORKDIR /app
COPY ./target/*.jar /app.jar
ENTRYPOINT ["java","-jar","/app.jar"]